#!/usr/bin/env python
"""@package docstring
This module handles collecting large sets of stock fundamental data
"""
import sys, os, datetime, re, signal, argparse
import cPickle as pickle
import pandas as pd
from infoFetcher import StockInfo, getHTMLWorker

class CollectionHandler(object):
    stepSize = 1;
    
    def __init__(self, storePath='store.h5'):
        """ Initialize the pool we will be using """
        self.storePath = storePath

    
    def getAllEqList(self):
        lines = open('companylist.csv').readlines()#getHTMLWorker(("http://www.nasdaq.com/screening/companies-by-region.aspx?&render=download", None));

        tkrBlackList = ["BLW", # A fund from citigroup
                        "CBIN",
                        "EFM",
                        "FUND",
                        "DGLD",
                        "ADVS",
                        "CKSW",
                        "MIG",
                        "MERU",
                        'QEPM',
                        'RALY',
                        'SPPR',
                        'SWH',
                        'MPV',
                        'SMI',
                        'ETAK',
                        'TVIZ',
                        'VIIX',
                        'USAS',
                        'ALO',
                        'GLDI',
                        'SLVO',
                        'BMLP',
                        #'ANCI',
                        #'ARCP',
                        'AB',
                        'ALLY',
                        'ALNA',
                        'AMEH',
                        'ATAC',
                        'BOXL',
                        'BURG',
                        'DVMT',
                        'ITEK',
                        'LTEA', # We make tea but our company name is long blockchain corp...

                    ]
        compBlackList = ["Credit Suisse",
                         "Treasury",
                         "VelocityShares",
                         "Trust",
                         "Trsy",
                         "Barclays",
                         "Treasury",
                         "region",
        ]
        tmpTkrList = [];
        lines = lines[1:-1];
        for line in lines:
            tmpInfo = line.split("\",");
            for i in range(len(tmpInfo)):
                tmpInfo[i] = tmpInfo[i].replace("\"", "");
            tmpTkr = tmpInfo[0];
            if ("/" not in tmpTkr) and ("^" not in tmpTkr) and len(tmpTkr)<5 and tmpTkr not in tkrBlackList and tmpInfo[3] != "0" and tmpInfo[-3] != "n/a" and "Fund" not in tmpInfo[1] and all([re.search(comp, tmpInfo[1], re.I)==None for comp in compBlackList]):
                tmpTkrList.append(tmpTkr.replace(" ", ""));

        return sorted(tmpTkrList);

    def fetchInitialInfo(self):
        """ Fetch all the info from all stocks listed on AMEX, NASDAQ, and NYSE. """

        # First get all the stocks we already have information for so we dcan remove them.
        alreadyProcTkrs = []
        if os.path.exists(self.storePath):
            fundDF = pd.read_hdf(self.storePath, 'fundDF')
            insiderDF = pd.read_hdf(self.storePath, 'insiderDF')

            alreadyProcTkrs = [stock.ticker for stock in self.dfToStockInfo(fundDF, insiderDF)]

        failListFName = "failList.pkl"
        failList = []
        if os.path.exists(failListFName):
            failF = open(failListFName);
            failList = pickle.load(failF)
            alreadyProcTkrs.extend([s.ticker for s in failList]);
            failF.close()
        #print 'Already processed stocks', alreadyProcTkrs
        #print 'Stocks that had a failure', [s.ticker for s in failList]
            
        store = pd.HDFStore(self.storePath)
        allStocks = ['WDC']#self.getAllEqList()
        fullSList = [tkr for tkr in allStocks if tkr not in alreadyProcTkrs]
        
        for step in range(len(fullSList)/self.stepSize+1):
            print len(fullSList)/self.stepSize+1, step
            sList = fullSList[step*self.stepSize:(step+1)*self.stepSize]
            if sList==[]:
                continue
            print sList

            tmpList = [StockInfo(tkr) for tkr in sList];
            infoList = [stock for stock in tmpList if stock.getInfoSuccess == '']
            failList.extend([stock for stock in tmpList if stock.getInfoSuccess != ''])
          
            #print "failures", [s.ticker for s in failList]
            if len(infoList) > 0:
                try:
                    fundDF, insiderDF = self.stockInfoToDF(infoList);
                    store.put('fundDF', fundDF, format='table', append=True, min_itemsize={'compName':100, 'sector':50, 'industry':50})
                    store.put('insiderDF', insiderDF, format='table', append=True, min_itemsize={'tkr':7})
                except Exception as e:
                    print "Problem storing stock. Adding it to fail list and moving on."
                    print e
                    failList.extend(infoList)
            #print "dumping fail list"
            origSigHandler = signal.signal(signal.SIGINT, signal.SIG_IGN)
            failF = open(failListFName, "w");
            pickle.dump(failList, failF)
            failF.close()
            signal.signal(signal.SIGINT, origSigHandler)


    def dfToStockInfo(self, fundDF, insiderDF):
        """ Convert a pandas data frame with all the information into a StockInfo object. """
        tickers = list(set(fundDF['tkr']))
        print tickers
        stocks = []
        for ticker in tickers:
            stock = StockInfo(ticker, initialFetch=False);
            sFunds = fundDF[fundDF['tkr'] == ticker]
            stock.cik = sFunds['cik'][0]
            stock.fiscYearEnds = sFunds['fiscEnd'][0]
            stock.compName = sFunds['compName'][0]
            stock.sector = sFunds['sector'][0]
            stock.industry = sFunds['industry'][0]
            histInfo = {};
            for date in sFunds['repDate']:
                histInfo[date] = {};
                tempFundInfo = sFunds[sFunds['repDate'] == date]
                #print tempFundInfo
                histInfo[date]= tempFundInfo.to_dict('records')[0]
                histInfo[date].pop('tkr')
                histInfo[date].pop('repDate')
                histInfo[date].pop('fiscEnd')
                histInfo[date].pop('compName')
                histInfo[date].pop('sector')
                histInfo[date].pop('industry')
                histInfo[date].pop('cik')
            stock.histInfo = histInfo

            insiderTrans = {};
            sInsider = insiderDF[insiderDF['tkr'] == ticker]
            for date in sInsider['date']:
                insiderTrans[date] = {};
                tempInfo = sInsider[sInsider['date'] == date]
                insiderTrans[date]= tempInfo.to_dict('records')[0]
                insiderTrans[date].pop('tkr')
                insiderTrans[date].pop('date')
            stock.insiderTransactions = insiderTrans
            stocks.append(stock);
        return stocks

    def stockInfoToDF(self, stocks):
        """ Convert the transient StockInfo objects into a permanent pandas data frame. """
        mets = sorted(stocks[0].histInfo.values()[0].keys())
        if stocks[0].insiderTransactions == {}:
            insiderKeys = []
        else:
            insiderKeys = sorted(stocks[0].insiderTransactions.values()[0].keys())

        fundDF = pd.DataFrame(columns=['repDate', 'tkr', 'cik', 'fiscEnd', 'compName', 'sector', 'industry']+mets);
        insiderDF = pd.DataFrame(columns=['date', 'tkr']+insiderKeys);
        for sI in range(len(stocks)):
            stock = stocks[sI]
            prevDatesLen = 0
            if sI > 0:
                prevDatesLen = len(stocks[sI-1].histInfo.keys())
            dates = sorted(stock.histInfo.keys())
            transDates = sorted(stock.insiderTransactions.keys())
            for dateIndex in range(len(dates)):
                date = dates[dateIndex]
                metVals = [stock.histInfo[date][met] for met in mets]
                fundDF.loc[dateIndex+(sI*prevDatesLen)] = [date, stock.ticker, stock.cik, stock.fiscYearEnds, stock.compName, stock.sector, stock.industry]+metVals
            for dateIndex in range(len(transDates)):
                metVals = [stock.insiderTransactions[transDates[dateIndex]][insiderKey] for insiderKey in insiderKeys]
                insiderDF.loc[dateIndex+(sI*len(dates))] = [transDates[dateIndex], stock.ticker]+metVals
                
        return fundDF, insiderDF

    def updateInfo(self):
        """ Only update a pandas data frame if necessary. """
        fundDF = pd.read_hdf(self.storePath, 'fundDF')
        insiderDF = pd.read_hdf(self.storePath, 'insiderDF')
        stocks = self.dfToStockInfo(fundDF, insiderDF)
        updatedStocks = []
        for stock in stocks:
            stock.update()

        newFundDF, newInsiderDF = self.stockInfoToDF(stocks);

        if os.path.exists(self.storePath.replace('.h5', 'Update.h5')):
            os.unlink(self.storePath.replace('.h5', 'Update.h5'))
        store = pd.HDFStore(self.storePath.replace('.h5', 'Update.h5'))
        store.put('fundDF', newFundDF, format='table', append=True)
        store.put('insiderDF', newInsiderDF, format='table', append=True)

    def fix(self):
        """ This just fixes or augments data in the collection. """
        fundDF = pd.read_hdf(self.storePath, 'fundDF')
        insiderDF = pd.read_hdf(self.storePath, 'insiderDF')
        stocks = self.dfToStockInfo(fundDF, insiderDF)

        # Delete old store if one exist. Note the name of the store is different from the original.
        if os.path.exists(self.storePath.replace('.h5', 'Fix.h5')):
            os.unlink(self.storePath.replace('.h5', 'Fix.h5'))
        store = pd.HDFStore(self.storePath.replace('.h5', 'Fix.h5'))
        
        for step in range(len(stocks)/self.stepSize+1):
            sList = stocks[step*self.stepSize:(step+1)*self.stepSize]
            if sList==[]:
                continue
            for stock in sList:
                print stock.ticker
                stock.calcPullsAndGains()

            try:
                newFundDF, newInsiderDF = self.stockInfoToDF(sList);
                store.put('fundDF', newFundDF, format='table', append=True, min_itemsize={'compName':100, 'sector':50, 'industry':50})
                store.put('insiderDF', newInsiderDF, format='table', append=True, min_itemsize={'tkr':7})
            except Exception as e:
                print "Problem storing stock. Adding it to fail list and moving on."
                print e
            
        
if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Fetch or update stock fundamental data.')
    parser.add_argument('--storePath', type=str, help='Path to store the h5 file to.', default="/Users/whopkins/newEqDB/fundInfo.h5")
    parser.add_argument('--update', action='store_true', help='Flag to only update.', default=False)
    parser.add_argument('--fix', action='store_true', help='Call the fix function which alters existing data.', default=False)

    args = parser.parse_args()

    c = CollectionHandler(storePath=args.storePath);
    if args.update:
        c.updateInfo();
    if args.fix:
        c.fix();
    if not args.update and not args.fix:
        c.fetchInitialInfo();

