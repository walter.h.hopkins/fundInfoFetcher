#!/usr/bin/env python
"""@package docstring
This module handles collecting large sets of stock fundamental data
"""

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
import math

def rejectOutliers(data, m=2):
    """ Removes outlier that are 2 sigma beyond the mean. """
    return data[abs(data - np.nanmean(data)) < m * np.nanstd(data)]

def plotBestMets(storePath, sigGain=0.5, bkgGain=0, minSep=0.2, minDataPoints=10):
    fundDF = pd.read_hdf(storePath, 'fundDF')
    insiderDF = pd.read_hdf(storePath, 'insiderDF')

    mets = ['% of price-to-industry']#'price to revenuePull']#, 'price/revenue ratioPull']#, 'price to revenue', 'FutureGain']
    #print fundDF[fundDF['tkr'] == 'WDC'][['repDate', 'price to revenue', 'price/revenue ratio', 'total revenue', 'revenue per share']]
    #print "\n".join(list(fundDF))
    #print fundDF['price to revenuePull']
    seps = []
    bestMets = []
    bestSeps = []
    for met in list(fundDF):
        if met == 'repDate' or met == 'tkr' or met == 'compName' or met == 'fiscEnd' or met == 'sector' or met == 'industry' or met == 'cik' or met == 'FutureGain':
            continue

        priceRevSig = fundDF[fundDF['FutureGain'] > sigGain][met]
        priceRevBkg = fundDF[fundDF['FutureGain'] < bkgGain][met]

        priceRevSig = priceRevSig[~np.isnan(priceRevSig)]
        priceRevBkg = priceRevBkg[~np.isnan(priceRevBkg)]

        priceRevSig = rejectOutliers(priceRevSig, m=2)
        priceRevBkg = rejectOutliers(priceRevBkg, m=2)
        # print met
        sigBkgSep = -1
        try:
            sigBkgSep = abs(np.nanmean(priceRevSig.values)-np.nanmean(priceRevBkg.values))/(math.sqrt(np.nanstd(priceRevSig.values)**2+np.nanstd(priceRevBkg.values)**2))
        except:
            continue
        if sigBkgSep == np.NaN:
            continue

        seps.append(sigBkgSep)
        if sigBkgSep < minsep or len(priceRevSig) < minDataPoints or len(priceRevSig)<minDataPoints:
            continue

        print "signal median", met, round(np.nanmean(priceRevSig.values),2), round(np.nanstd(priceRevSig.values),2), round(np.nanmin(priceRevSig.values),2), round(np.nanmax(priceRevSig.values),2)
        print "bkg median", met, round(np.nanmean(priceRevBkg.values),2), round(np.nanstd(priceRevBkg.values),2), round(np.nanmin(priceRevBkg.values),2), round(np.nanmax(priceRevBkg.values),2)

        minX = min(np.nanmin(priceRevSig.values), np.nanmin(priceRevBkg.values))
        maxX = max(np.nanmax(priceRevSig.values), np.nanmax(priceRevBkg.values))
        bestSeps.append(met)
        bestMets.append(met)

        plt.clf()
        plt.figure();
        priceRevSig.hist(bins=20, range=(minX,maxX), histtype='step', normed=1, label='gain>%.2f' % sigGain)
        priceRevBkg.hist(bins=20, range=(minX,maxX), histtype='step', normed=1, label='gain<%.2f' % bkgGain)
        leg = plt.legend(loc=1, borderaxespad=0.)
        plt.xlabel(met,horizontalalignment='right', x=1.0)
        plt.ylabel('Arbitrary units')
        plt.savefig('plots/'+met.replace(' ', '_').replace('/','_to_')+'.pdf', bbox_inches='tight')

    print max(seps)
    print bestMets
    print bestSeps

if __name__ == '__main__':
     parser = argparse.ArgumentParser(description='Find the metrics with the best separation between stocks that have some gain vs stocks that have some loss.')
    parser.add_argument('--storePath', type=str, help='Location of the full stock info file.', default="/Users/whopkins/newEqDB/fundInfoFix.h5")
    parser.add_argument('--sigGain', type=float, help='Signal gain in fractions', default=0.5)
    parser.add_argument('--bkgGain', type=float, help='Background gain in fractions', default=0)
    args = parser.parse_args()
    
    plotBestMets(args.storePath, sigGain=args.sigGain, bkgGain=args.bkgGain)
