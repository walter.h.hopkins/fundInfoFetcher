# Fundamental Stock Info Fetcher

This project pulls fundamental stock information from various sources and converts them to a date indexed dictionary. Currently the infoFetcher.py script does the scraping and organization of the information from one stock. The CollectionHandler will be able to auto update the info when necessary and eventually calculate metrics of collections of stocks (S&P 500, industries, sectors, etc). The main source of information comes from annual reports. 

The current information that is fetched is:
* Historical insider transactions from insider-monitor.com.
* Current information from yahoo.
* Historical annual income/balance sheets, valuation information from both morningstar and advfn


## Planned feature additions
1. Auto updates by the collection handler. The collection handler would be a singleton that handles the organization and storage of all data. 

## Planned tests and performance enhancements
1. Compare pandas with other options. ROOT (from CERN) is very quick with embarrasingly parallel data but I would prefer avoiding ROOT. The data set should easily reach the multi GBs and fast selection of subsets is imperative.


## Immediate to-do list
1. Start writing data analyzer to test data subset

# Stock Data Analyzer

Once the data set has been defined. A small testing data set will be produced to start writing an analyzer which will plot signal vs background histograms of various metrics. Signal stocks are defined to be ones with capital gains of more than a certain percentage (e.g. 20%). The percentage will depend on the size of the data set. For example, a 100% capital gains requirement would probably result in a too small signal data set. The size of the data set will also determined what kind of machine learning will be possible. The most likely viable method will be supervised learning with a boosted decision tree. 