#!/usr/bin/env python
# Written by Walter Hopkins (walter.h.hopkins@gmail.com)

import multiprocessing as mp

from bs4 import BeautifulSoup

import sys, datetime, time, re, copy, json, numpy, signal, requests, random

from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

def getHTMLWorker(urlInfo, maxTries=5, randomTime=5):
    """ Worker function that uses PhantomJS browser to scrape information from various web pages
    It's outside the StockInfo class to allow for multiprocessing which greatly aids the speed of 
    info fetching. The input is a tuple consisting of a URL and an XPATH of the element to wait for. 
    If the XPATH is None no wait is used. Yahoo price data is a special case that is treated differently."""
    url, waitForElement, getInfoFromEl = urlInfo
    driver = webdriver.PhantomJS(service_args=['--ignore-ssl-errors=true', "--load-images=no"])
    #driver.implicitly_wait(10) 

    if "query1.finance.yahoo.com" in url:
        # First get the cookie with phantomjs
        gotCookie = False;
        nAttempts = 0;
        while nAttempts < maxTries:
            try:    
                ticker = url.split("download/")[1].split("?")[0]
                driver.get("https://finance.yahoo.com/quote/"+ticker+"/history?p="+ticker)
                crumbEl = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, "//a[contains(@href, 'crumb')]")))
                crumbURL = crumbEl.get_attribute("href")
                crumb = crumbURL.split("&crumb=")[1]
                url+="&crumb="+crumb
                gotCookie = True
                break;
            except Exception as cookieExcept:
                print "Error retrieving cookie."
                html = "Problem getting cookie"
                print "The error was", cookieExcept
                if nAttempts < maxTries:
                    print "Trying again. This is attempt", nAttempts
                nAttempts +=1
                time.sleep(random.randint(0, randomTime))

        nAttempts = 0
        if gotCookie:
            while nAttempts < maxTries:
                try:
                    session = requests.Session()
                    cookies = driver.get_cookies()
                    for cookie in cookies: 
                        session.cookies.set(cookie['name'], cookie['value'])
                    response = session.get(url)
                    html = response.content.split("\n")
                    break;
                except Exception as priceExcept:
                    print "Error, can't get price data. This is attempt", nAttempts
                    print url
                    html = "Problem loading page."
                    print priceExcept
                    nAttempts+=1
                    time.sleep(random.randint(0, randomTime))
                
    elif 'download' in url and 'nasdaq' in url:
        session = requests.Session()
        response = session.get(url)            
        html = response.content.split("\n")
    else:
        
        nAttempts = 0;
        while nAttempts < maxTries:
            try:
                driver.get(url)
                if waitForElement==None:
                    html = driver.page_source.encode('ascii', 'ignore')
                else:
                    element = WebDriverWait(driver, 15).until(EC.presence_of_element_located((By.XPATH, waitForElement)))
                    if getInfoFromEl:
                        html = element.get_attribute('innerHTML').encode('ascii', 'ignore')
                    else:
                        html = driver.page_source.encode('ascii', 'ignore')#
                break;
            except Exception as getExcept:
               print "Error, can't get html data. This is attempt", nAttempts
               html = "Problem loading page"
               print url
               print getExcept
               nAttempts+=1
               time.sleep(random.randint(0, randomTime))

        html = html.replace("&lt;","<").replace("&gt;", ">").replace("&amp;", "&")
    driver.service.process.send_signal(signal.SIGTERM) 
    
    return html

class StockInfo(object):
    """Contains all necessary data for a specific stock, both historical and current. 
    This class fetches informations from various sites, verifies the information against other source, 
    and augments the information with new time wise relative information (deviations from mean values). 
    """
    priceAvgPeriod = 10;

    insiderTransSS = '\<tr\>\<td\>(.+?)\<\/td\>\<td class\="tdnum buy"\>(.+?)\<\/td\>\<td class\="tdnum buy"\>.*?\<\/td\>\<td class\="tdnum sale"\>(.+?)\<\/td\>\<td class\="tdnum sale">.*?\<\/td\>\<td class\="tdnum opt"\>(.+?)\<\/td\>\<td class\="tdnum opt">.*?\<\/td\>\<\/tr\>'

    # Dictionary mapping morning star labels to advfn labels. We need this to make sure the data isn't garbage.
    mornADVFN = {
        'revenue':['total revenue', 'total interest income'],
        #'operating income':'operating income',
        #'working capital':['working capital'],
        'dividends':['dividends paid per share (dps)', 'dividends paid per share'],
    }

    # Metrics we don't care about and that cause various problems
    blackList = [
        'template indicator',
        'format indicator',
        'preliminary full context ind',
        'projected fiscal year date',
        'earnings period indicator',
        'quarterly indicator',
        'auditor report',
        'basic earnings indicator',
        'date preliminary data loaded',
        'inventory valuation method',
        'auditor name',
        'auditors name',
        'auditors report',
        'projected fiscal year end date',
    ]

    
    # Search strings used to get company name, sector, and industry from yahoo
    basicSearchStrs = {'compName':'longName\"\:\"(.+?)\"', 
                       'sector':'sector":"(.*?)"', 
                       'industry':'industry":"(.*?)"',
                       'fiscYearEnds':'"lastFiscalYearEnd":.*?"fmt":"\d*-(.*?)"',
                       'avgVolume':'averageVolume":\{"raw":(.+?),', 
    }
    
    maxPoolSize = 8;
    timeOut = 300;
    volumeLowerLimit = 50000
    
    def __init__(self, ticker, initialFetch=True):
        """ Initialize the stock info fetcher with information that is unique to one instance of the fetcher. 
        This mainly initialized information unique to a particular stock."""

        print 'Initializing everything for', ticker
        self.ticker              = ticker        
        self.compName            = ''
        self.sector              = ''
        self.industry            = ''
        self.fiscYearEnds        = ''
        self.getInfoSuccess      = ''
        self.cik                 = ''
        self.mornValData         = {}
        self.mornFundData        = {}
        self.insiderTransactions = {}
        self.histInfo            = {}
        
        # URLs to get historical info from
        # The tuple consists of url, xpath for element to wait for, whether to use the element text.
        # If the last tuple element is False the full page html source is used. 
        self.urls = [
            ("http://financials.morningstar.com/ajax/keystatsAjax.html?t="+self.ticker+"&region=USA&culture=en-us", "//pre", True),
            ("http://financials.morningstar.com/valuation/valuation-history.action?t="+self.ticker+"&productCode=COM&type=price-sales", "//table[@id='valuation_history_table']", True),
        ]

        # ADVFN is treated in a special way
        self.advfnQRURL = ("http://www.advfn.com/stock-market/NASDAQ/"+self.ticker+"/financials?btn=quarterly_reports&mode=company_data", "//input[@value='quarterly_reports']", False)

        # This is meant to pick out the correct row in the valuation metric HTML
        self.valCompSearchStr = self.ticker+'(.+)Price\/\S+ for '

        if initialFetch:
            # We first need to get the company name to look up the CIK (in case the ticker doesn't work)
            self.yahooHTML = getHTMLWorker(("http://finance.yahoo.com/quote/"+self.ticker+"?p="+self.ticker, None, False))
            if not self.fetchBasicData():
                print "Average volume is too low, less than", self.volumeLowerLimit
                print "Marking this as a problem."
                self.getInfoSuccess = 'volumeLessThan_%.2f$' % self.volumeLowerLimit;
                return

            # Get CIK before anything (we will need it for other metrics)
            if not self.getCIK():
                print "Can't get CIK number... not finishing the initiral fetch."
                self.getInfoSuccess = 'noCIK';
                return
            self.urls.append(("http://www.insider-monitor.com/trading/cik"+self.cik+".html", None, False))

            # Get the number of pages we need from advfn. This depends on whether we are updating.
            self.addADVFNURLs()

            # Get current and historical data
            self.getInfo();

    # These functions are used to make it possible to pickle the info. This is needed because we don't want to pickle all the html
    def __getstate__(self):
        """Return state values to be pickled. This function is mainly used for debugging failed fetches. """
        stateVals = (self.ticker,
                     self.cik,
                     self.getInfoSuccess,
                     self.compName,
                     self.industry,
                     self.sector,
                     self.fiscYearEnds,
                     self.histInfo,
                     self.urls,
                     self.mornValData,
                     self.mornFundData,
                     self.insiderTransactions,
        )
        return stateVals

    def __setstate__(self, state):
        """Restore state from the unpickled state values."""
        (self.ticker,
         self.cik,
         self.getInfoSuccess,
         self.compName,
         self.industry,
         self.sector,
         self.fiscYearEnds,
         self.histInfo,
         self.urls,
         self.mornValData,
         self.mornFundData,
         self.insiderTransactions,
        ) = state
        
    def fetchBasicData(self):
        """ Scrape the basic information (company name, sector, industry, fiscal year) from yahoo. """
        basicInfo = {}
        for met in self.basicSearchStrs:
            basicInfo[met] = ''
            reComp = re.compile(self.basicSearchStrs[met], re.S|re.I)
            try:  
                metricVal = reComp.search(self.yahooHTML).group(1).rstrip().lstrip()   
            except Exception as findExc:
                print findExc
                metricVal = 'NI'
                print "Can't find", met
            basicInfo[met] = metricVal
            
        self.compName     = basicInfo['compName']
        self.sector       = basicInfo['sector']
        self.industry     = basicInfo['industry']
        self.fiscYearEnds = basicInfo['fiscYearEnds']

        try:
            avgVolume = float(basicInfo['avgVolume'])
        except Exception as e:
            print "Error converting average volume to a float"
            print e
            return False;
        return avgVolume > self.volumeLowerLimit
        
        
    def getCIK(self):
        """ Function to find the Central Index Key (CIK) for a particular ticker. The SEC has a nice lookup."""
        urls =[("https://www.sec.gov/cgi-bin/browse-edgar?CIK="+self.ticker+"&owner=exclude&action=getcompany", None, False),
               ("https://www.sec.gov/cgi-bin/cik_lookup?company="+self.compName, None, False),
               ("https://www.sec.gov/cgi-bin/cik_lookup?company="+self.ticker, None, False),
            ]
        reComps = [re.compile("CIK=(\d+)\&", re.S|re.I),
                   re.compile("CIK=(\d+)\"", re.S|re.I),
                   re.compile("CIK=(\d+)\"", re.S|re.I),
                   ]

        foundCIK = False
        for sI in range(len(urls)):
            html = getHTMLWorker(urls[sI])
            matchTKR = reComps[sI].search(html)
            if matchTKR != None:
                self.cik = matchTKR.group(1).lstrip("0");
                foundCIK = True
                break;
        if not foundCIK:
            print "Sorry, I really couldn't find the CIK for this company."
        return foundCIK
       

    def addADVFNURLs(self):
        """ Function that gets a list of URLs for the pages with the fundamental data for a particular ticker. 
        ADVFN divides this data and puts it on several different pages. We of course want all the data. 
        This is done for annual but not the quarterly reports for which only the latest page is used. 
        The function returns the page that contains the last five years of information which is used in the update function."""

        # Start getting annual reports
        url = "http://fr.advfn.com/bourses/NASDAQ/"+self.ticker.upper()+"/bilan-comptable?btn=start_date&start_date=1&mode=annual_reports"
        html = getHTMLWorker((url, "//input[@value='annual_reports']", False))
        reComp = re.compile("option\s+value=\"(\d+)\"", re.S|re.I)
        result = reComp.findall(html)
        if result != []:
            pageList = [result[i] for i in range(len(result)) if (i-1) % 5 == 0]
            for pageNum in pageList:
                self.urls.append((url.replace("start_date=1", "start_date="+pageNum), "//input[@value='annual_reports']", False))
        else:
            self.urls.append((url, "//input[@value='annual_reports']", False))

        # Get the latest quarterly report for TTM calculations
        self.urls.append(self.advfnQRURL)

    
    def update(self):
        """ This method updates the data with new annual and quarterly reports as well as new price data. 
        Insider transactions are always updated."""
        today = datetime.datetime.now()
        latestDate, lastAnRepDate = sorted(self.histInfo.keys(), reverse=True)[:2]
        deltaAnRep = (today-lastAnRepDate)
        print lastAnRepDate
        deltaTTM = (today-latestDate)
        updated = False

        self.insiderHTML = getHTMLWorker(("http://www.insider-monitor.com/trading/cik"+self.cik+".html", None, False))
        self.exInsiderData(self.insiderHTML)

        # We need to update price info only
        if deltaTTM.days < 120 and deltaTTM.days > self.priceAvgPeriod:
            print "Updating price info only."
            self.histInfo[today] = self.histInfo[latestDate]
            self.histInfo.pop(latestDate);
            self.priceReportDate(reqDates=[today]);
            self.calcPriceRatios();
            self.calcPullsAndGains();
            print sorted(self.histInfo.keys())
            updated = True
            
        # We need to update the quarterly reports
        elif deltaTTM.days > 120:
            self.advfnQRHTML = [getHTMLWorker(self.advfnQRURL)];            
            self.advfnQRData = self.exADHistData(self.advfnQRHTML, 'quarter end date', makeTTM=True)
            self.histInfo[today] = self.advfnQRData
            self.histInfo.pop(latestDate);
            self.priceReportDate(reqDates=[today]);
            self.calcPriceRatios();
            self.calcPullsAndGains();
            updated = True
            
        # We need to grab new annual reports if the last one has been 13 months ago
        elif deltaAnRep.days>390:
            # It doesn't take much longer to update everything rather than a subset of the annual reports so just update everything. 
            self.addADVFNURLs()
            self.getInfo(getInsider=False);
            updated = True
        
        return updated


    def exInsiderData(self, html):
        """ Scrape insider data. """
        reComp = re.compile(self.insiderTransSS, re.S|re.I)
        insiderTransactions = {}

        try:
            tempInfo = reComp.findall(html)
            for temp in tempInfo:
                transDate = datetime.datetime.strptime(temp[0].lstrip().rstrip(), '%Y-%m');
                insiderTransactions[transDate] = {'buys':int(temp[1].replace(",","")),
                                                       'sells':int(temp[2].replace(",","")),
                                                       'optEx':int(temp[3].replace(",",""))
                                                       }                                                                                          
        except Exception as e:
            print "Couldn't fetch insider info due to error."
            print e
            insiderTransactions = {};

        return insiderTransactions

    def exADHistData(self, htmls, repType, makeTTM=False):
        """ Extract historical info from ADVFN. """
        outData = {}
        if len(htmls) == 1:
            if "no financial data available" in htmls[0].lower() or "aucune donne financire" in htmls[0].lower():
                return outData
        fundInfo = {}
        for html in htmls:
            # If something went wrong fetching the html give up
            if html == "Problem loading page":
                return {}
            soup = BeautifulSoup(html)
            tables = soup.findChildren('table')
            fundTableIndex = 5;
            try:
                fundTable = tables[fundTableIndex]
            except Exception as e:
                print "Problem getting fifth table."
                print e
            for row in fundTable.findChildren('tr'):
                tds = row.findChildren('td');
                metName = tds[0].string
                if metName == None or any([badMet in metName.lower() for badMet in self.blackList]):
                    continue;
                metName = metName.lstrip().rstrip().lower()
                vals = [td.string for td in tds[1:]]
                if len(vals) == 0:
                    continue;
                if metName not in fundInfo:
                    fundInfo[metName] = vals;
                else:
                    fundInfo[metName].extend(vals)
                    
        # Done extracting all the data. Now let's organize it by date
        for dI in range(len(fundInfo[repType])):
            if fundInfo[repType][dI] == None:
                continue;
            tempDate = datetime.datetime.strptime(fundInfo[repType][dI].lstrip().rstrip(), '%Y/%m')
            outData[tempDate] = {}

            for metName in fundInfo:
                if repType in metName:
                    continue
                # try to convert the info to a number. If it doesn't work just use the string
                try:
                    val = float(fundInfo[metName][dI].replace(",",""))
                except Exception as convExcept:
                    if fundInfo[metName][dI] == None:
                        val = numpy.nan
                    else:
                        print metName
                        val = fundInfo[metName][dI];
                strMetName = str(metName)
                outData[tempDate][strMetName] = val

        # Sum the data (this is for TTM and obviously won't work for ratios which we will calculate ourselves anyway)
        if makeTTM:
            ttmDates = sorted(outData.keys(), reverse=True)[:4]
            ttmData = {}
            for metName in fundInfo:
                if not all([metName in outData[date] for date in ttmDates]):
                    continue
                vals=[outData[date][metName] for date in ttmDates]
                if any([not isinstance(val, float) for val in vals]):
                    continue;
                if "As %" in metName or "Ratio" in metName or "Per" in metName:
                    val = numpy.nanmean(vals)
                else:
                    val = sum(vals)

                ttmData[metName] = val
            outData = ttmData
            
        return outData

    def exMornHistData(self):
        """ Extract historical info from Morningstar. """
        fundInfo = {}
        try:
            soup = BeautifulSoup(self.mornKeyStatsHTML)
        except Exception as e:
            print "Problem fetching morningstar key stats"
            print e
            return fundInfo
        [s.extract() for s in soup("span")]
        tables = soup.findChildren('table')
        
        for fundTable in [tables[0],tables[2]]:
            for row in fundTable.findChildren('tr'):
                tds = row.findChildren(['td']);
                ths = row.findChildren('th')
                if len(ths)==1:
                    metName = ths[0].string.lower().rstrip().lstrip()
                    vals = [td.string.encode('ascii', 'ignore') for td in tds]                
                elif len(ths)>1:
                    metName = "year end date"
                    vals = [th.string.encode('ascii', 'ignore') for th in ths[1:]]                

                if len(vals) == 0 or vals == [None]:
                    continue;
                
                if metName not in fundInfo:
                    fundInfo[metName] = vals;                
                    
        # Done extracting all the data. Now let's organize it by date
        mornFundData = {}
        for dI in range(len(fundInfo['year end date'])):
            if fundInfo['year end date'][dI] == None:
                continue;
            if fundInfo['year end date'][dI].lstrip().rstrip() == 'TTM':
                tempDate = datetime.datetime.now();
            else:
                tempDate = datetime.datetime.strptime(fundInfo['year end date'][dI].lstrip().rstrip(), '%Y-%m')
            mornFundData[tempDate] = {}

            for metName in fundInfo:
                # try to conver the info to a number. If it doesn't work just use the string
                try:
                    val = float(fundInfo[metName][dI].replace(",",""))
                except Exception as e:                    
                    if fundInfo[metName][dI] == '':
                        val = 0;
                    else:
                        val = fundInfo[metName][dI];
                mornFundData[tempDate][metName] = val
        return mornFundData
    
    def exMornValData(self, dates):
        """ Extract valuation info from Morningstar. """
        soup = BeautifulSoup(self.mornValueHTML)
        [s.extract() for s in soup("span")]
        table = soup.findChildren('tbody')[0]
        valInfo  = {}
        for row in table.findChildren('tr'):
            tds = row.findChildren(['td','th']);
            if 'abbr' not in tds[0].attrs:
                continue;
            if "for" in tds[0].attrs['abbr'] and "500" not in tds[0].string:
                metName = tds[0].attrs['abbr'].split("for")[0].rstrip().lstrip().lower();
                vals = []
                for td in tds[1:]:
                    try:
                        val = float(td.string);
                    except:
                        val = td.string;
                    vals.append(val)
                
                valInfo[metName] = vals
                
        mornValData = {}
        for dI in range(len(dates)):
            mornValData[dates[dI]] = {}
            for metName in valInfo.keys():
                mornValData[dates[dI]][metName] = valInfo[metName][dI]
        return mornValData

    def compMornADVFN(self):
        """ Compare Morningstar data with ADVFN data. They of course should match. 
        One known bug at ADVFN is that the price/sales data is wrong by three orders
        of magnitude. This won't bother us since we have the price data and will calculate 
        price/sales our selves. """
        window = 0.25
        dates = sorted(self.mornFundData.keys());
        for dI in range(len(dates)-1):
            date = dates[dI]
            if date not in self.advfnFundData.keys():
                continue;
            for metName in self.mornADVFN:
                advfnValue = 0;
                for key in self.mornADVFN[metName]:
                    if key in self.advfnFundData[date]:
                        advfnValue =  self.advfnFundData[date][key]
                        break;
                if self.mornFundData[date][metName] == 0:
                    if self.mornFundData[date][metName] != advfnValue:
                        print "Warning", metName, "doesn't match for", date,":", self.mornFundData[date][metName], advfnValue
                        return False;
                elif (self.mornFundData[date][metName]-advfnValue)/self.mornFundData[date][metName] > window:
                    print "Warning", metName, "doesn't match for", date,":", self.mornFundData[date][metName], advfnValue
                    return False;
        return True;

    def priceReportDate(self, reqDates = []):
        """ Get the prices information close to an annual or quarterly report so that we can calculate price ratios ourselves.
        We will fetch the price two months after the fiscal year ends. These reports take some time to come out
        and we don't want to be sensitive to earnings release fluctuations. We will also average the price over 10 days
        to avoid sensitivity to fluctuations. """
        dateURLs = [];
        dates = [];
        if reqDates == []:
            reqDates = self.histInfo.keys();
        
        ordDates = []
        print "Getting prices for", len(reqDates), 'dates'
        for date in reqDates:
            newDate = date+datetime.timedelta(days=62)
            if newDate.weekday() >= 6:
                newDate += datetime.timedelta(days=2)
            if newDate > datetime.datetime.now():
                newDate = datetime.datetime.now()-datetime.timedelta(days=11)
            startDate = newDate#-datetime.timedelta(days=self.priceAvgPeriod)
            epoch = datetime.datetime.utcfromtimestamp(0)
            endSecs = str(int(((newDate+datetime.timedelta(days=self.priceAvgPeriod))-epoch).total_seconds()))
            startSecs = str(int((newDate-epoch).total_seconds()))
            dateURLs.append(('https://query1.finance.yahoo.com/v7/finance/download/'+self.ticker+'?period1='+startSecs+'+&period2='+endSecs+'&interval=1d', None, False)
            )
            dates.append(newDate);
            ordDates.append(date)

        origSigHandler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        htmlPool = mp.Pool(self.maxPoolSize)
        signal.signal(signal.SIGINT, origSigHandler)
        priceResult = []

        try:
            tempRes = htmlPool.map_async(getHTMLWorker, dateURLs, callback=priceResult.extend);
            tempRes.get(self.timeOut)
        except KeyboardInterrupt:
            print "You pushed CTL-C, terminating workers."
            htmlPool.terminate()
        else:
            htmlPool.close()
        htmlPool.join()
   
        for resI in range(len(priceResult)):
            res = priceResult[resI]
            
            try:
                prices = [float(price.split(',')[-2]) for price in res[1:] if price !='']
                price = numpy.nanmean(prices)
            except:
                price = numpy.nan
                
                print "Couldn't convert price to number."
                
            self.histInfo[ordDates[resI]]['price'] = price
            
    def calcPriceRatios(self):
        """ Calculate ratios with respect to share price. Examples that are commonly uses are price/earnings per share 
        and price/sales per share. """
        mets = self.histInfo.values()[0].keys();
        for met in mets:
            if "per share" in met and 'Pull' not in met:
                for date in sorted(self.histInfo.keys()):
                    newMet = "price to "+met.split(" per share")[0]
                    if self.histInfo[date][met] != 0:
                        self.histInfo[date][newMet] = self.histInfo[date]['price']/self.histInfo[date][met]
                    else:
                        self.histInfo[date][newMet] = numpy.nan
                    #print self.ticker, date, newMet, round(self.histInfo[date][newMet],2), round(self.histInfo[date]['price'],2), self.histInfo[date][met]

    def calcPullsAndGains(self):
        """ Calculate the pulls for all the metrics with historical data. Here pull is similar to 
        a signed chi^2  and is defined as (x-mean)/(sigma) where x is the current value of a metric
        and mean is the historical mean and sigma is the historical sigma. """
        sortedDates = sorted(self.histInfo.keys());
        for dI in range(len(sortedDates)):
            date = sortedDates[dI]
        
            for met in self.histInfo[date].keys():
                if isinstance(self.histInfo[date][met], float) and 'Pull' not in met and 'FutureGain' not in met:
                    # Also calculate the gains for the price
                    if met == 'price':
                        if dI < len(sortedDates)-1:
                            self.histInfo[date]["FutureGain"] = (self.histInfo[sortedDates[dI+1]]['price']-self.histInfo[date]['price'])/self.histInfo[date]['price']
                        else:
                            self.histInfo[date]["FutureGain"] = numpy.nan
                    valList = [];
                    for pI in range(dI):
                        prevDate = sortedDates[pI]
                        if len(valList) >= 10:
                            break;
                        if (type(self.histInfo[prevDate][met])==float or type(self.histInfo[prevDate][met])==numpy.float64) and not numpy.isnan(self.histInfo[prevDate][met]):
                            valList.append(self.histInfo[prevDate][met]);

                    #print self.ticker, len(valList), valList
                    if len(valList) < 3:
                        self.histInfo[date][met+"Pull"] = numpy.nan;
                        continue;
                   
                    std = numpy.nanstd(valList);
                    avg = numpy.nanmean(valList);
                    if std > 0:
                        pull = (self.histInfo[date][met]-avg)/std
                    else:
                        pull = numpy.nan

                    self.histInfo[date][met+"Pull"] = pull;
                    

        
    def getHTML(self, getHist=True):
        """ Grab all the HTML pages to search for data! """
       
        #urlList = priceURL+self.urls
        urlList = self.urls
 
        origSigHandler = signal.signal(signal.SIGINT, signal.SIG_IGN)
        htmlPool = mp.Pool(self.maxPoolSize)
        signal.signal(signal.SIGINT, origSigHandler)
        htmlResult = []

        try:
            tempRes = htmlPool.map_async(getHTMLWorker, urlList, callback=htmlResult.extend);
            tempRes.get(self.timeOut)
        except KeyboardInterrupt:
            print "You pushed CTL-C, terminating workers."
            htmlPool.terminate()
        else:
            htmlPool.close()
        htmlPool.join()
   
        [self.mornKeyStatsHTML, 
         self.mornValueHTML,
         self.insiderHTML] = htmlResult[:3]
        self.advfnHTML = htmlResult[3:-1]
        self.advfnQRHTML = htmlResult[-1:]
        self.mornKeyStatsHTML = json.loads(self.mornKeyStatsHTML)['ksContent'];
        
                               
    def getInfo(self, getHist=True, getInsider=True):
        """ Extract all the desirable information from all the HTML. """
        print "Grabbing HTML for", self.ticker
        self.getHTML(getHist);
        
        print "Done grabbing HTML"
        if getHist:
            # Get the annual report data
            print "Processing ADVFN data"
            self.advfnFundData = self.exADHistData(self.advfnHTML, 'year end date')
            
            if self.advfnFundData == {}:
                print "There was no annual report financial data. This will be marked as bad but may not be. "
                self.getInfoSuccess = 'noAnnualReports'
                return
            
            print "Processing morningstar hist data"
            self.mornFundData = self.exMornHistData()
            if self.mornFundData == {}:
                self.getInfoSuccess = 'noMornKeyStats'
                return
            
            print "Processing morningstar value data"
            self.mornValData = self.exMornValData(sorted(self.mornFundData.keys()))
            if self.mornValData == {}:
                self.getInfoSuccess = 'noMornValData'
                return

            # We don't return for missing insider transactions (yet)
            if getInsider:
                print "Processing insider data"
                self.insiderTransactions = self.exInsiderData(self.insiderHTML)
                if self.insiderTransactions == {}:
                    print "No insider transaction"

            if not self.compMornADVFN():
                print "Problem: morningstar and ADVFN data doesn't match. Marked as bad."
                self.getInfoSuccess = 'mornADVFNMisMatch'
                return 

            print "Processing quarterly reports"
            # Now get the quarterly report data
            self.advfnQRData = self.exADHistData(self.advfnQRHTML, 'quarter end date', makeTTM=True)
            if self.advfnQRData == {}:
                print "No quarterly data was found. Marking this as a failure but it may not be. "
                self.getInfoSuccess = 'noQuarterlyInfo'
                return 
            print "Done processing data"
            self.histInfo = self.advfnFundData
            now = datetime.datetime.now()
            self.histInfo[now] = self.advfnQRData

            print "Getting price data for report dates"
            self.priceReportDate()
            print "Calculating price ratios"
            self.calcPriceRatios()
            print "Calculating pulls"
            self.calcPullsAndGains();

   
